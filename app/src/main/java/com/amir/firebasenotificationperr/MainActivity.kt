package com.amir.firebasenotificationperr

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.util.Log
import android.view.View
import android.widget.Toast
import androidx.lifecycle.lifecycleScope
import com.amir.firebasenotificationperr.databinding.ActivityMainBinding
import com.amir.firebasenotificationperr.ui.UserListActivity
import com.google.android.gms.tasks.OnCompleteListener
import com.google.firebase.database.FirebaseDatabase
import com.google.firebase.installations.FirebaseInstallations
import com.google.firebase.messaging.FirebaseMessaging
import kotlinx.coroutines.launch
import kotlinx.coroutines.tasks.await

class MainActivity : AppCompatActivity() {
    private lateinit var binding: ActivityMainBinding
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding= ActivityMainBinding.inflate(layoutInflater)
        setContentView(binding.root)

        init()

    }

    private fun init(){

        binding.tvDone.setOnClickListener(View.OnClickListener {
            Log.i("svvvvsvs", "init: "+2424)

            lifecycleScope.launch {
                val map: HashMap<String, Any> = HashMap()

                map["token"] = FirebaseMessaging.getInstance().token.await()
                val data = FirebaseDatabase.getInstance().reference.child("User")
                    .child(binding.etNumber.getText().toString())
                    .setValue(map)
                    .await()
            }
            startActivity(Intent(this,UserListActivity::class.java))

        })
    }

}