package com.amir.firebasenotificationperr.model

data class NotificationReq(
    var notification: Notification = Notification(),
    var data : Notification.ShowData = Notification.ShowData(),
    var to: String = ""

)
{
    data class Notification(
        var body: String = "",
        var title: String = "",
        var image: String = "",
        var click_action: String = ""

    )

    {
        data class ShowData(
            var key1: String = "",
            var key2: String = ""
        )
    }
}

