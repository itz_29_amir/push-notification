package com.amir.firebasenotificationperr.network

import com.amir.firebasenotificationperr.model.NotificationReq
import com.amir.firebasenotificationperr.model.NotificationResponse
import retrofit2.Call
import retrofit2.http.Body
import retrofit2.http.Headers
import retrofit2.http.POST


interface NotificationRequest {
     @Headers(
         value = [ "Content-Type:application/json",
             "Authorization:key=AAAAbX_eGqQ:APA91bFQ_Km-_JWiUUn-9Rejv73Bp2yXqDYTy5ApX8Q0jhzoLq-4294GzHUukGu_m4aKES-bxjfkVlcXdmRGoCTu3ZPo-qL00gsLO2WIGNbuWx5eY2gRUob6_QpYaV6DMKsvs03ITD6Z"
         ]
     )
     @POST("send")
     suspend fun sent(@Body req: NotificationReq): NotificationResponse
}