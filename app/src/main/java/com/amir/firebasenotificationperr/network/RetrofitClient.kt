package com.amir.firebasenotificationperr.network

import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory

object RetrofitClient {
    fun getInstance() = Retrofit.Builder().baseUrl("https://fcm.googleapis.com/fcm/")
        .addConverterFactory(GsonConverterFactory.create())
        .build()


}