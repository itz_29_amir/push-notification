package com.amir.firebasenotificationperr.network.adapter

import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.firebase.ui.database.FirebaseRecyclerAdapter
import com.firebase.ui.database.FirebaseRecyclerOptions
import android.view.LayoutInflater
import com.amir.firebasenotificationperr.databinding.UserBinding
import com.amir.firebasenotificationperr.model.User

class UserListAdapter(options: FirebaseRecyclerOptions<User>, val callback : (String) -> Unit,
                      val tokenkey : (String) -> Unit) : FirebaseRecyclerAdapter<User, UserListAdapter.UserListAdapter_View>(
    options
) {
    private lateinit var binding: UserBinding

    class UserListAdapter_View(itemView: View) :
        RecyclerView.ViewHolder(itemView)

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): UserListAdapter_View {

        binding= UserBinding.inflate(LayoutInflater.from(parent.context),parent,false)
        return UserListAdapter_View(binding.root)
         }

    override fun onBindViewHolder(holder: UserListAdapter_View, position: Int, model: User) {

        binding.tvNumber.text=getRef(position).key

        val key  = getRef(position).key
        binding.cv.setOnClickListener(View.OnClickListener {
              callback(model.token.toString())
            tokenkey(key!!)
        })
    }

}