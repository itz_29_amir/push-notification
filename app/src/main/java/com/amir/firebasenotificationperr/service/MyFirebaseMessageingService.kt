package com.amir.firebasenotificationperr.service

import com.amir.firebasenotificationperr.utils.NoficationHelper
import com.google.firebase.messaging.FirebaseMessagingService
import com.google.firebase.messaging.RemoteMessage




class MyFirebaseMessageingService : FirebaseMessagingService() {

    override fun onMessageReceived(remoteMessage: RemoteMessage) {
        super.onMessageReceived(remoteMessage)

        val body: String? = remoteMessage.notification?.body
        val title: String? = remoteMessage.notification?.title
        val image: String? = remoteMessage.notification?.imageUrl.toString()
        val clickAction: String? = remoteMessage.notification?.clickAction

        val key1: String? =  remoteMessage.data.get("key1")
        val key2: String? =  remoteMessage.data.get("key2")

        val helper = NoficationHelper(this)
        helper.tiggerNotification(title,body,image,clickAction,key1,key2)

    }
}