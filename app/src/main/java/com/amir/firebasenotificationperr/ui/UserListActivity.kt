package com.amir.firebasenotificationperr.ui

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.util.Log
import android.widget.Toast
import androidx.lifecycle.lifecycleScope
import androidx.recyclerview.widget.LinearLayoutManager
import com.amir.firebasenotificationperr.databinding.ActivityUserListBinding
import com.amir.firebasenotificationperr.model.NotificationReq
import com.amir.firebasenotificationperr.model.User
import com.amir.firebasenotificationperr.network.NotificationRequest
import com.amir.firebasenotificationperr.network.RetrofitClient
import com.amir.firebasenotificationperr.network.adapter.UserListAdapter
import com.firebase.ui.database.FirebaseRecyclerOptions
import com.google.firebase.database.*
import com.google.firebase.database.FirebaseDatabase.getInstance
import kotlinx.coroutines.launch
import kotlinx.coroutines.tasks.await


class UserListActivity : AppCompatActivity() {
     private lateinit var adapter: UserListAdapter
    private lateinit var binding : ActivityUserListBinding
//    val list =listOf<User>()
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding= ActivityUserListBinding.inflate(layoutInflater)
        setContentView(binding.root)

        init();
    }

    private fun init(){

        val key1 : String? =intent.getStringExtra("key1")
        val key2 : String? =intent.getStringExtra("key2")

        Toast.makeText(this, "$key1+ $key2", Toast.LENGTH_SHORT).show()
        binding.rvUser.setLayoutManager(LinearLayoutManager(this@UserListActivity))

        val query: Query = getInstance().getReference().child("User")

        val options: FirebaseRecyclerOptions<User> = FirebaseRecyclerOptions.Builder<User>()
            .setQuery(query, User::class.java)
            .build()
        Log.i("svvssv", "init: "+options.toString())

        adapter=UserListAdapter(options,{
            Log.i("dsfsss", "init: "+it.toString() )

        } ,{
            sentByRest(it)

        })
        binding.rvUser.setAdapter(adapter )

    }

    private fun sentByRest(s: String) {

       lifecycleScope.launch {
           val await = FirebaseDatabase.getInstance().getReference()
               .child("User")
               .child(s)
               .get().await()

           val req = NotificationReq(
               NotificationReq.Notification(
                   "Aamir",
                   "Arsenal goal in added time, score is now 3-0",
                   "https://image.shutterstock.com/image-photo/kiev-ukraine-may-14-2016-260nw-420838831.jpg",
                   "my_click"
               ),NotificationReq.Notification.ShowData("value1","value2"),
               await.child("token").getValue().toString()

           )

           val res = RetrofitClient.getInstance().create(NotificationRequest::class.java).sent(req)

       }



    }

    override fun onStart() {
        super.onStart()
        adapter.startListening()
    }

    override fun onStop() {
        super.onStop()
        adapter.stopListening()
    }
}

