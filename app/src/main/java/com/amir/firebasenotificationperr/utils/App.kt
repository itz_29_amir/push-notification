package com.amir.firebasenotificationperr.utils

import android.app.Application
import android.app.NotificationManager

import android.app.NotificationChannel
import android.os.Build


class App : Application() {

    override fun onCreate() {
        super.onCreate()
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
            val notificationChannel = NotificationChannel(
               ID,
                NAME,
                NotificationManager.IMPORTANCE_DEFAULT
            )
            notificationChannel.description =DESCRIPTION
            val manager = getSystemService(
                NotificationManager::class.java
            )
            manager.createNotificationChannel(notificationChannel)
        }
    }
}