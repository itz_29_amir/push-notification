package com.amir.firebasenotificationperr.utils

import android.R.id

import androidx.core.app.NotificationManagerCompat

import android.R

import android.app.PendingIntent
import android.content.Context

import android.content.Intent
import android.graphics.BitmapFactory

import android.graphics.Bitmap
import androidx.core.app.NotificationCompat
import java.io.IOException
import java.io.InputStream
import java.net.HttpURLConnection
import java.net.URL


class NoficationHelper (val context: Context) {

    private val id = 100

    fun tiggerNotification(
        title: String?,
        body: String?,
        image: String?,
        clickAction: String?,
        key1: String?,
        key2: String?
    ) {
        val intent = Intent(clickAction)
        intent.putExtra("key1", key1)
        intent.putExtra("key2", key2)
        val pendingIntent =
            PendingIntent.getActivity(context, 100, intent, PendingIntent.FLAG_UPDATE_CURRENT)
        val builder: NotificationCompat.Builder = NotificationCompat.Builder(context, ID)
            .setContentTitle(title)
            .setContentText(body)
            .setSmallIcon(R.mipmap.sym_def_app_icon)
            .setPriority(NotificationCompat.PRIORITY_DEFAULT)
            .setStyle(NotificationCompat.BigPictureStyle().bigPicture(getBitmapFromURL(image)))
            .setContentIntent(pendingIntent)
        val managerCompat = NotificationManagerCompat.from(context)
        managerCompat.notify(id, builder.build())
    }


    fun getBitmapFromURL(src: String?): Bitmap? {
        return try {
            val url = URL(src)
            val connection: HttpURLConnection = url.openConnection() as HttpURLConnection
            connection.setDoInput(true)
            connection.connect()
            val input: InputStream = connection.getInputStream()
            BitmapFactory.decodeStream(input)
        } catch (e: IOException) {
            e.printStackTrace()
            null
        }
    }
}